module Tests

open Xunit
open Return
open RateLinesProvider
open RateLinesParser

let urlRight = "https://www.cnb.cz/en/financial-markets/foreign-exchange-market/central-bank-exchange-rate-fixing/central-bank-exchange-rate-fixing/daily.txt"
let urlWrong = "https://www.cnb.cz2"
let baseCurrencyCode = "CZK"


let CurrencyCodes = [ 
    "USD"; 
    "EUR";
    "CZK"; 
    "JPY"; 
    "KES";
    "RUB";
    "THB";
    "TRY";
    "XYZ";
    ]


[<Fact>]
let ``Test GenerateExhangeRates Right`` () =
    let linesResult = FetchLines urlRight
    let exchangeRatesPlain = GenerateExchangeRatePlains linesResult.ReturnValue.Value CurrencyCodes 
    let rates = GenerateExhangeRates exchangeRatesPlain baseCurrencyCode |> Seq.toArray 
    Assert.True(rates.Length = 6)

[<Fact>]
let ``Test GenerateExhangeRates Wrong`` () =
    let result = FetchLines urlWrong
    Assert.False(result.ReturnType = returnType.Success, result.ReturnMessage)

