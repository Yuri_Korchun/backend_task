﻿module RateLinesProvider

open Return
open System
open System.Net


let CreateStreamReader url = 
    try 
        let req = WebRequest.Create(Uri(url)) 
        let resp = req.GetResponse() 
        let stream = resp.GetResponseStream() 
        new IO.StreamReader(stream) |> ReturnOk 
    with 
        | ex -> 
            ReturnError returnType.SystemError (ex.Message + ": [" + url + "]")

let FetchLinesFromStreamReader (streamReader : IO.StreamReader ) = 
    seq {       
    while not streamReader.EndOfStream do
        yield streamReader.ReadLine()
    }

let FetchLines url = 
    let streamReaderResult = CreateStreamReader url
    match streamReaderResult.ReturnType with 
        | Success -> 
            FetchLinesFromStreamReader streamReaderResult.ReturnValue.Value |> ReturnOk 
        | _ -> 
            ReturnError streamReaderResult.ReturnType streamReaderResult.ReturnMessage
