﻿module ExchangeRateProvider

open System.Collections.Generic

open Return
open RateLinesProvider
open RateLinesParser

let GetExchangeRates ( url:string ) ( baseCurrencyCode:string ) currencyCodes = 
    let linesResult = FetchLines url 
    match linesResult.ReturnType with 
        | Success ->
             let exchangeRatePlains = GenerateExchangeRatePlains linesResult.ReturnValue.Value currencyCodes 
             let exchangeRates = GenerateExhangeRates exchangeRatePlains baseCurrencyCode
             let res = exchangeRates |> Seq.filter (fun rateResult -> IsSuccess rateResult)
             res |> Seq.map (fun exchangeRate -> exchangeRate.ReturnValue.Value) |> ReturnOk 
        | _ ->  
             ReturnError linesResult.ReturnType linesResult.ReturnMessage


