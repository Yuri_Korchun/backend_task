﻿open System.Collections.Generic

open Return
open ExchangeRateProvider

let urls = [
    "https://www.cnb.cz/en/financial-markets/foreign-exchange-market/central-bank-exchange-rate-fixing/central-bank-exchange-rate-fixing/daily.txt";
    "https://www.cnb.cz/en/financial-markets/foreign-exchange-market/fx-rates-of-other-currencies/fx-rates-of-other-currencies/fx_rates.txt";
]

let baseCurrencyCode = "CZK"

let CurrencyCodes = [ 
    "USD"; 
    "EUR";
    "CZK"; 
    "JPY"; 
    "KES";
    "RUB";
    "THB";
    "TRY";
    "XYZ";
    ]

[<EntryPoint>]
let main argv =
    
    let totalRetrieved = new List<string>()

    for url in urls  do
        let ratesResult = GetExchangeRates url baseCurrencyCode CurrencyCodes
        match ratesResult.ReturnType with 
            | Success ->
                let rates = ratesResult.ReturnValue.Value |> Seq.toArray 
                printfn "\nRetrieved [%d] exchange rates from url [%s]:\n" rates.Length url
                for rate in rates  do
                    printfn "%s/%s = %f" rate.SourceCurrency.CurrencyCode rate.TargetCurrency.CurrencyCode rate.Value
                totalRetrieved.AddRange(rates |> Seq.map ( fun rate -> rate.SourceCurrency.CurrencyCode ) |> Seq.toList)  
            | _ -> 
                printfn "\nCould not retrieve rates from url [%s] due to of error: [%s] [%s]\n" url (ReturnTypeToString ratesResult.ReturnType)  ratesResult.ReturnMessage
    
    let nonRetrieved = 
        CurrencyCodes 
        |> Seq.filter (fun currency -> currency <> baseCurrencyCode) 
        |> Seq.filter (fun currency -> not (totalRetrieved.Exists( fun code -> code = currency))) 
        |> Seq.toList
   
    printfn "\nDid not retrieve [%d] exchange rates for source currencies:\n" nonRetrieved.Length
    for code in nonRetrieved do 
        printfn "%s" code
    
    0 // return an integer exit code
