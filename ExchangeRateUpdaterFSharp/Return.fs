﻿module Return

type returnType = 
    | Success
    | SystemError
    | InputError
    | ConversionError
    | GeneralError
    
type Return<'T> = 
    { ReturnValue: 'T option
      ReturnType:  returnType
      ReturnMessage: string }

let ReturnOk returnValue = { ReturnValue = Some returnValue;   ReturnType = Success;  ReturnMessage = ""; }

let ReturnError returnType returnMessage  = { ReturnValue = None;   ReturnType = returnType;  ReturnMessage = returnMessage; }

let IsSuccess (ret: Return<'T>) = 
    match ret.ReturnType with 
        |  Success -> true
        | _ -> false

let ReturnTypeToString (_returnType : returnType) = 
    match _returnType with 
        | Success -> "Success"
        | SystemError -> "SystemError"
        | InputError -> "InputError"
        | GeneralError -> "GeneralError"
        | ConversionError -> "ConversionError"
        | _ -> "UnknownError"




