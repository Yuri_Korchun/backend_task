﻿// see also 
// http://stackoverflow.com/questions/4949941/convert-string-to-system-datetime-in-f
// convenient, functional TryParse wrappers returning option<'a>

module TryParser 

open Return

let tryParseWith (tryParseFunc: string -> bool * _) = tryParseFunc >> function
    | true, v    -> v |> ReturnOk 
    | false, _   -> ReturnError returnType.ConversionError "Conversion error"

let TryParseDate   = tryParseWith System.DateTime.TryParse
let TryParseInt    = tryParseWith System.Int32.TryParse
let TryParseSingle = tryParseWith System.Single.TryParse
let TryParseDouble = tryParseWith System.Double.TryParse
let TryParseDecimal = tryParseWith System.Decimal.TryParse


