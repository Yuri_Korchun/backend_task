﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExchangeRateUpdater;
using System.Collections.Generic;

namespace ExchangeRateUpdaterTest
{
    [TestClass]
    public class ExchangeRateUpdaterTests
    {
        private string _rightUrl =
            "https://www.cnb.cz/en/financial-markets/foreign-exchange-market/central-bank-exchange-rate-fixing/central-bank-exchange-rate-fixing/daily.txt";

        private string _wrongUrl = "https://www.cnb1.cz";

        private Currency[] currencies = new[]
         {
                new Currency("USD"),
                new Currency("EUR"),
                new Currency("CZK"),
                new Currency("JPY"),
                new Currency("KES"),
                new Currency("RUB"),
                new Currency("THB"),
                new Currency("TRY"),
                new Currency("XYZ")
            };

        private List<string> _linesRight = new List<string>() {
        "27 Sep 2019 #188",
        "Country|Currency|Amount|Code|Rate",
        "Australia|dollar|1|AUD|15.964",
        "Brazil|real|1|BRL|5.681",
        "Bulgaria|lev|1|BGN|13.212",
        "Canada|dollar|1|CAD|17.832",
        "China|renminbi|1|CNY|3.316",
        "Croatia|kuna|1|HRK|3.487",
        "Denmark|krone|1|DKK|3.461",
        "EMU|euro|1|EUR|25.840",
        "Hongkong|dollar|1|HKD|3.015",
        "Hungary|forint|100|HUF|7.701",
        "Iceland|krona|100|ISK|19.155",
        "IMF|SDR|1|XDR|32.251",
        "India|rupee|100|INR|33.527",
        "Indonesia|rupiah|1000|IDR|1.668",
        "Israel|new shekel|1|ILS|6.783",
        "Japan|yen|100|JPY|21.861",
        "Malaysia|ringgit|1|MYR|5.643",
        "Mexico|peso|1|MXN|1.204",
        "New Zealand|dollar|1|NZD|14.863",
        "Norway|krone|1|NOK|2.606",
        "Philippines|peso|100|PHP|45.548",
        "Poland|zloty|1|PLN|5.891",
        "Romania|leu|1|RON|5.437",
        "Russia|rouble|100|RUB|36.727",
        "Singapore|dollar|1|SGD|17.114",
        "South Africa|rand|1|ZAR|1.568",
        "South Korea|won|100|KRW|1.972",
        "Sweden|krona|1|SEK|2.415",
        "Switzerland|franc|1|CHF|23.795",
        "Thailand|baht|100|THB|77.098",
        "Turkey|lira|1|TRY|4.177",
        "United Kingdom|pound|1|GBP|29.104",
        "USA|dollar|1|USD|23.633"
        };

        private List<string> _linesWrong = new List<string>() {
        "27 Sep 2019 #188",
        "Country|Currency|Amount|Code|Rate",
        "Australia|dollar|1|AUD|15.964",
        "USA|dollar|1|USD|23.633"
        };


        [TestMethod]
        public void Test_Init_Right()
        {
            var url = _rightUrl;
            var provider = new ExchangeRateProvider("CZK", url);
            Assert.IsTrue(provider.Success, provider.Error);
        }

        [TestMethod]
        public void Test_Init_Wrong()
        {
            var url = _wrongUrl;
            var provider = new ExchangeRateProvider("CZKK", url);
            Assert.IsTrue(!provider.Success, provider.Error);
        }


        [TestMethod]
        public void Test_CheckCurrency_Right()
        {
            var url = _rightUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);

            var currencyCheckResult = provider.CheckCurrencyCode("CZK");
            Assert.IsTrue(currencyCheckResult.IsSuccess, currencyCheckResult.Error);
        }

        [TestMethod]
        public void Test_CheckCurrency_Wrong()
        {
            var url = _wrongUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);

            var currencyCheckResult = provider.CheckCurrencyCode("CZ");
            Assert.IsTrue(currencyCheckResult.IsFailure, currencyCheckResult.Error);
        }

        [TestMethod]
        public async Task Test_FetchLinesFromUrlAsync_Right()
        {
            var url = _rightUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);
      
            var result = await provider.FetchLinesFromUrlAsync(url).ConfigureAwait(false);
            Assert.IsTrue(result.IsSuccess, result.Error);
            Assert.IsTrue(result.Value.Any(), result.Error);
        }

        [TestMethod]
        public async Task Test_FetchLinesFromUrlAsync_Wrong()
        {
            var url = _wrongUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);

            var result = await provider.FetchLinesFromUrlAsync(url).ConfigureAwait(false);
            Assert.IsFalse(result.IsSuccess, result.Error);
        }

        [TestMethod]
        public void Test_ParseExchangeRatesFromLines_Right()
        {
            var url = _rightUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);

            var ratesResult = provider.ParseExchangeRatesFromLines(currencies, _linesRight);
            Assert.IsTrue(ratesResult.IsSuccess, ratesResult.Error);
            Assert.IsTrue(ratesResult.Value.Count == 6, ratesResult.Error);
        }


        [TestMethod]
        public void Test_ParseExchangeRatesFromLines_Wrong()
        {
            var url = _wrongUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);

            var ratesResult = provider.ParseExchangeRatesFromLines(currencies, _linesWrong);
            Assert.IsFalse(ratesResult.Value.Count == 6, ratesResult.Error);
        }

        [TestMethod]
        public async Task Test_ParseLinesFromUrlAsync_Right()
        {
            var url = _rightUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);

            var ratesResult = await provider.FetchLinesFromUrlAsync(url).ConfigureAwait(false);
            Assert.IsTrue(ratesResult.IsSuccess, ratesResult.Error);
            Assert.IsTrue(ratesResult.Value.Any(), ratesResult.Error);
        }

        [TestMethod]
        public async Task Test_ParseLinesFromUrlAsync_Wrong()
        {
            var url = _wrongUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);


            var ratesResult = await provider.FetchLinesFromUrlAsync(url).ConfigureAwait(false);
            Assert.IsFalse(ratesResult.IsSuccess, ratesResult.Error);

            ratesResult = await provider.FetchLinesFromUrlAsync(url);
            Assert.IsFalse(ratesResult.IsSuccess, ratesResult.Error);
        }

        [TestMethod]
        public void Test_ParseRateFromLine_Right()
        {
            var url = _rightUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);

            var line = "USA|dollar|1|USD|23.514";

            var currencies = new[]
            {
                new Currency("USD"),
                new Currency("EUR"),
                new Currency("CZK"),
                new Currency("JPY"),
                new Currency("KES"),
                new Currency("RUB"),
                new Currency("THB"),
                new Currency("TRY"),
                new Currency("XYZ")
            };

            var rateResult = provider.ParseRateFromLine(line, currencies.ToList());

            Assert.IsTrue(rateResult.IsSuccess, rateResult.Error);
            Assert.IsTrue(rateResult.Value.SourceCurrency.Code == "USD", $"Wrong Source Currency Code - must be 'USD', but [{rateResult.Value.SourceCurrency.Code}]");
            Assert.IsTrue(rateResult.Value.TargetCurrency.Code == "CZK", $"Wrong Target Currency Code - must be 'CZK', but [{rateResult.Value.TargetCurrency.Code}]");
            Assert.IsTrue(rateResult.Value.Value == 23.514M, $"Wrong Rate value - must be '23.514', but [{rateResult.Value.Value}]");
        }

        [TestMethod]
        public void Test_ParseRateFromLine_Wrong()
        {
            var url = _rightUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);

            var line = "Australia|dollar|1|AUD|15.977";


            var currencies = new[]
            {
                new Currency("USD"),
                new Currency("EUR"),
                new Currency("CZK"),
                new Currency("JPY"),
                new Currency("KES"),
                new Currency("RUB"),
                new Currency("THB"),
                new Currency("TRY"),
                new Currency("XYZ")
            };

            var rateResult = provider.ParseRateFromLine(line, currencies.ToList());

            Assert.IsTrue(rateResult.IsFailure, rateResult.Error);
        }

        [TestMethod]
        public void Test_GetExchangeRatesAsync_Right()
        {
            var url = _rightUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);

            var currencies = new[]
            {
                new Currency("USD"),
                new Currency("EUR"),
                new Currency("CZK"),
                new Currency("JPY"),
                new Currency("KES"),
                new Currency("RUB"),
                new Currency("THB"),
                new Currency("TRY"),
                new Currency("XYZ")
            };
            var rates = provider.GetExchangeRatesAsync(currencies, url);
            Assert.IsTrue(provider.Success, provider.Error);
        }

        [TestMethod]
        public async Task Test_GetExchangeRatesAsync_Wrong()
        {
            var url = _wrongUrl;
            var provider = new ExchangeRateProvider(
                "CZK",
                url);

            var currencies = new[]
            {
                new Currency("USD"),
                new Currency("EUR"),
                new Currency("CZK"),
                new Currency("JPY"),
                new Currency("KES"),
                new Currency("RUB"),
                new Currency("THB"),
                new Currency("TRY"),
                new Currency("XYZ")
            };
            var rates = await provider.GetExchangeRatesAsync(currencies, url).ConfigureAwait(false);
            Assert.IsFalse(provider.Success, provider.Error);
        }

        [TestMethod]
        public async Task Test_ProcessUrlAsync_Right()
        {
            var url = _rightUrl;

            var currencies = new[]
            {
                new Currency("USD"),
                new Currency("EUR"),
                new Currency("CZK"),
                new Currency("JPY"),
                new Currency("KES"),
                new Currency("RUB"),
                new Currency("THB"),
                new Currency("TRY"),
                new Currency("XYZ")
            };

            var rateResult = await Program.ProcessUrlAsync(url, currencies, "CZK");

            Assert.IsTrue(rateResult.Any(), "Empty Rates");
        }

        [TestMethod]
        public async Task Test_ProcessUrlAsync_Wrong()
        {
            var url = _wrongUrl;

            var currencies = new[]
            {
                new Currency("USD"),
                new Currency("EUR"),
                new Currency("CZK"),
                new Currency("JPY"),
                new Currency("KES"),
                new Currency("RUB"),
                new Currency("THB"),
                new Currency("TRY"),
                new Currency("XYZ")
            };

            var rateResult = await Program.ProcessUrlAsync(url, currencies, "CZK");

            Assert.IsFalse(rateResult.Any(), "Must be empty rates");
        }


    }


}