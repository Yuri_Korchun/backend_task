﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ExchangeRateUpdater
{
    public class ExchangeRateProvider
    {
        /// <summary>
        /// Should return exchange rates among the specified currencies that are defined by the source. But only those defined
        /// by the source, do not return calculated exchange rates. E.g. if the source contains "EUR/USD" but not "USD/EUR",
        /// do not return exchange rate "USD/EUR" with value calculated as 1 / "EUR/USD". If the source does not provide
        /// some of the currencies, ignore them.
        /// </summary>

        public string Url { get; private set; }

        public Currency BaseCurrencyCode { get; private set; }

        public bool Success { get; private set; }

        public string Error { get; private set; }
        public string DeclarationHeader { get; private set; }

        public ExchangeRateProvider(string baseCurrencyCode, string url)
        {
            Init(baseCurrencyCode, url);
        }

        private void Init(string baseCurrencyCode, string url)
        {
            var currencyCodeCheckResult = CheckCurrencyCode(baseCurrencyCode);
            if (currencyCodeCheckResult.IsFailure)
            {
                Success = false;
                Error = currencyCodeCheckResult.Error;
                return;
            }

            BaseCurrencyCode = new Currency(baseCurrencyCode);
            Url = url;
            Success = true;
        }

        public Result<bool> CheckCurrencyCode(string currencyCode)
        {
            if (currencyCode.Trim().Length != 3)
            {
                return Result.Fail<bool>($"Length of currency code [{currencyCode}] must be 3!");
            }
            return Result.Ok(true);
        }

        public async Task<IEnumerable<ExchangeRate>> GetExchangeRatesAsync(IEnumerable<Currency> currencies, string url)
        {
            var exchangeRatesEmpty = new List<ExchangeRate>();

            if (!Success) return exchangeRatesEmpty;

            var linesResult = await FetchLinesFromUrlAsync(url).ConfigureAwait(false);

            if (linesResult.IsFailure)
            {
                Success = false;
                Error = $"{linesResult.Error}";
                return exchangeRatesEmpty;
            }

            var exchangeRatesResult = ParseExchangeRatesFromLines(currencies, linesResult.Value);

            Success = exchangeRatesResult.IsSuccess;
            Error = exchangeRatesResult.Error;

            return exchangeRatesResult.Value;
        }

        public Result<List<ExchangeRate>> ParseExchangeRatesFromLines(IEnumerable<Currency> currencies, IEnumerable<string> lines)
        {
            var exchangeRates = new List<ExchangeRate>();
            var count = 0;
            foreach (var line in lines)
            {
                count++;
                if (count == 1)
                {
                    DeclarationHeader = line;
                    continue;
                }

                if (count == 2) continue;

                var rateResult = ParseRateFromLine(line, currencies.ToList());
                if (rateResult.IsSuccess) exchangeRates.Add(rateResult.Value);
            }

            return Result.Ok(exchangeRates);

        }
        
        public async Task<Result<List<string>>> FetchLinesFromUrlAsync(string url)
        {
            var lines = new List<string>();
            try
            {
                var request = WebRequest.Create(url);
                var response = await request.GetResponseAsync().ConfigureAwait(false);
                var stream = response.GetResponseStream();

                if (stream == null)
                {
                    return Result.Fail<List<string>>($"Response stream is null");
                }

                var responseStream = new StreamReader(stream);

                var line = "";
                while ((line = responseStream.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }
            catch (Exception e)
            {
                return Result.Fail<List<string>>($"{e.Message}");
            }

            return Result.Ok(lines);
        }

        public Result<ExchangeRate> ParseRateFromLine(string line, List<Currency> currencies)
        {
            var exchangeRateSourceResult = ParseRateSourceFromLine(line, currencies);

            if (exchangeRateSourceResult.IsFailure)
                return Result.Fail<ExchangeRate>(exchangeRateSourceResult.Error);

            var exchangeRateSource = exchangeRateSourceResult.Value;

            var currencyCodeCheckResult = CheckCurrencyCode(exchangeRateSource.CurrencyCode);

            if (currencyCodeCheckResult.IsFailure)
                return Result.Fail<ExchangeRate>(currencyCodeCheckResult.Error);

            var sourceCurrency = new Currency(exchangeRateSource.CurrencyCode);

            if (!decimal.TryParse(exchangeRateSource.RateScale, out decimal rateScale))
                return Result.Fail<ExchangeRate>($"Can't extract RateScale from [{line}]");

            if (rateScale == 0) return Result.Fail<ExchangeRate>($"Parsed RateScale from [{line}] is 0");

            if (!decimal.TryParse(exchangeRateSource.RateValue, out decimal rateAmount))
                return Result.Fail<ExchangeRate>($"Can't extract RateAmount from [{line}]");

            decimal rateValue = rateAmount / rateScale;

            return Result.Ok(new ExchangeRate(sourceCurrency, BaseCurrencyCode, rateValue));

        }

        public Result<ExchangeRateSource> ParseRateSourceFromLine(string line, List<Currency> currencies)
        {
            var columns = line.Split('|');

            if (columns.Length != 5) return Result.Fail<ExchangeRateSource>($"Count of columns in {line} <> 5");

            var exchangeRateSource = new ExchangeRateSource(
                countryName: columns[0],
                currencyName: columns[1],
                rateScale: columns[2],
                currencyCode: columns[3],
                rateValue: columns[4]);
                        
            if (currencies.Any(x => x.Code == exchangeRateSource.CurrencyCode))
            {
                return Result.Ok(exchangeRateSource);
            }

            return Result.Fail<ExchangeRateSource>($"Currency code [{exchangeRateSource.CurrencyCode}] is excessive");
        }

    }
}