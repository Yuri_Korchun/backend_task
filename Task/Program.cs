﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRateUpdater
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
            string[] urls = {
            "https://www.cnb.cz/en/financial-markets/foreign-exchange-market/central-bank-exchange-rate-fixing/central-bank-exchange-rate-fixing/daily.txt",
            "https://www.cnb.cz/en/financial-markets/foreign-exchange-market/fx-rates-of-other-currencies/fx-rates-of-other-currencies/fx_rates.txt"
            };

            string baseCurrencyCode = "CZK";
            IEnumerable<Currency> currencies = new[]
            {
                new Currency("USD"),
                new Currency("EUR"),
                new Currency("CZK"),
                new Currency("JPY"),
                new Currency("KES"),
                new Currency("RUB"),
                new Currency("THB"),
                new Currency("TRY"),
                new Currency("XYZ")
            };
                        
            var currenciesList = currencies.Distinct().ToList();
            currenciesList.Remove(currenciesList.FirstOrDefault(p => p.Code == baseCurrencyCode));

            var totalProcessedCurrencies = new List<string>();

            foreach (var url in urls)
            {
                var processedCurrencies = await ProcessUrlAsync(url, currenciesList, baseCurrencyCode).ConfigureAwait(false);
                totalProcessedCurrencies.AddRange(processedCurrencies);
            }

            var nonProcessedCurrencies = currenciesList.Select(p => p.Code).Except(totalProcessedCurrencies).ToList();

            Console.WriteLine($"Non retrieved [{nonProcessedCurrencies.Count()}] exchange rates for source currencies:\n");
            foreach (var nonProcessed in nonProcessedCurrencies)
            {
                Console.WriteLine(nonProcessed);
            }

            Console.WriteLine("\nPress ENTER to EXIT");
            Console.ReadLine();
        }

        public static async Task<List<string>> ProcessUrlAsync(string url, IEnumerable<Currency> currencies, string baseCurrencyCode)
        {
            var processedCurrenciesList = new List<string>();
            try
            {
                var provider = new ExchangeRateProvider(baseCurrencyCode, url);
                if (!provider.Success)
                {
                    Console.WriteLine($"Provider [{url}] initialization error: '{provider.Error}'.");
                }

                var rates = await provider.GetExchangeRatesAsync(currencies, url).ConfigureAwait(false);
                if (!provider.Success)
                {
                    Console.WriteLine($"Could not successfully retrieve exchange rates: '{provider.Error}' from [{provider.DeclarationHeader} {url}].");
                }

                var ratesList = rates.ToList();

                if (ratesList.Any())
                {
                    Console.WriteLine($"Retrieved [{ratesList.Count()}] exchange rates from [{provider.DeclarationHeader}  {url}]:\n");
                    foreach (var rate in ratesList)
                    {
                        Console.WriteLine(rate.ToString());
                        processedCurrenciesList.Add(rate.SourceCurrency.Code);
                    }

                }
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Could not retrieve exchange rates: '{e.Message}' from [{url}].");
            }
            return processedCurrenciesList;
        }

    }
}
